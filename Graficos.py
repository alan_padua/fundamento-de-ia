import itertools
import os.path as path

import matplotlib.pyplot as plt
import numpy as np


class Graficos:

    def __init__(self):
        print("Classe Graficos")
        self.GALLERY = path.join("gallery")

    def visualizacaoScatter(self, data_input, y_encoder, label_x: int, label_y: int):
        plt.scatter(data_input[:, label_x], data_input[:, label_y], c=y_encoder, cmap='gist_rainbow')
        plt.xlabel('Length', fontsize=18)
        plt.ylabel('Width', fontsize=18)

    def plot_confusion_matrix(self, cm, classes,
                              normalize=False,
                              title='Confusion matrix',
                              cmap=plt.cm.Blues):
        """
        Esta função imprime e plota a matriz de confusão.
        A normalização pode ser aplicada definindo `normalize = True`.
        """
        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            print("Matriz de confusão normalizada")
        else:
            print('Matriz de confusão, sem normalização')

        print(cm)

        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')

        # Plot non-normalized confusion matrix
        plt.figure()
