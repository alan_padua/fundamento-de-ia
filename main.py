from AprendizadoNaoSupervisionado import AprendizadoNaoSuprevisionado
from AprendizadoSupervisionado import AprendizadoSupervisionado
from Graficos import Graficos
from Processamento import Processamento


class Main:

    def preProcessamento(self):
        """
        Preprocessamento

        :return:
        """
        processamento = Processamento('dataset/healthcare-dataset-stroke-data.csv')
        processamento.alterandoLabels()
        processamento.removerValores()
        processamento.removerColunasDesnecessarias()

        en_conjunto = processamento.encoderTransform()

        # Separando x e y
        x_encoder, y_encoder = processamento.separandoXeY(en_conjunto, labels_x=processamento.labels_x, labels_y=processamento.labels_y)
        print("Separando X e Y")
        print(x_encoder)
        print(y_encoder)

        #  Dimensionando cada feature para um determinado intervalo, padrão 0 e 1.
        #  Esta transformação é freqüentemente usada como uma alternativa para média zero e variância unitária.
        data_input, y_encoder = processamento.dimensionandoFeatures(y_encoder, x_encoder)
        print("Dimensionando Features")
        print(data_input.shape)
        print(y_encoder.shape)
        print(data_input.min())
        print(data_input.max())
        return x_encoder, y_encoder, data_input

    def visualizacaoScatter(self, data_input, y_encoder):
        """
        Gráficos
        :param data_input:
        :param y_encoder:

        :return:
        """
        graficos = Graficos()

        # Visualização Scatter
        print("Visualização Scatter")
        graficos.visualizacaoScatter(data_input, y_encoder, label_x=4, label_y=5)

    def aprendizadoSupervisionado(self, x_encoder, y_encoder):
        """
        Aprendizado Supervisionado
        :param x_encoder:
        :param y_encoder:

        :return:
        """
        graficos = Graficos()
        aprendizadoSupervisionado = AprendizadoSupervisionado()

        # Depois de fazer o pré-processamento de dados, agora é hora de dividirmos os dados em dados de treinamento e teste
        X_train, X_test, Y_train, Y_test = aprendizadoSupervisionado.separarDadosTreinamentoEValidacao(x_encoder, y_encoder)
        print("Separar Dados Treinamento e Validacao")
        print("X_train shape: {}", format(X_train.shape))
        print("Y_train shape: {}", format(Y_train.shape))
        print("X_test shape: {}", format(X_test.shape))
        print("Y_test shape: {}", format(Y_test.shape))

        # Regressão
        # Agora vamos construir um modelo de regressão logística
        conjunto_lr = aprendizadoSupervisionado.regressaoLogistica(X_train, Y_train)
        print("Regressao Logistica")
        print(conjunto_lr)

        # Predição
        y_hat = aprendizadoSupervisionado.predicao(conjunto_lr, X_test)
        print("Predição")
        print(y_hat)

        # Predição com probabilidade
        yhat_prob = aprendizadoSupervisionado.predicaoProbabilidade(conjunto_lr, X_test)
        print("Predição com probabilidade")
        print(yhat_prob)

        # Salvar Dados
        aprendizadoSupervisionado.salvarDados(conjunto_lr, 'logisticRegression.joblib')
        print("Salvar Dados")

        # Matriz de Confusão
        # Calcular matrix de Confução
        cnf_matrix = aprendizadoSupervisionado.calcularConfusaoMatrix(Y_test, y_hat)
        print("Calcular matrix de Confução")

        graficos.plot_confusion_matrix(cnf_matrix, classes=['stroke=1', 'no stroke=0'], normalize=False, title='Confusion matrix')

    def aprendizadoNaoSupervisionado(self, data_input, y_encoder):
        """
        Aprendizado não Supervisionado
        :param data_input:
        :param y_encoder:

        :return:
        """
        aprendizadoNaoSupervisionado = AprendizadoNaoSuprevisionado()

        # select k using the elbow method
        k = aprendizadoNaoSupervisionado.elbow(data_input, max_iter=100, tolerancia=0.6)
        print(f"k={k}")

        # Cluster analysis
        n_acertos = aprendizadoNaoSupervisionado.clusterAnalysis(y_encoder)
        print('Númeo de acertos: ' + str((n_acertos / len(y_encoder)) * 100))

        if k == None:
            k = 5
        kmeans = aprendizadoNaoSupervisionado.silhouette(data_input, k)
        print("Silhouette")

        aprendizadoNaoSupervisionado.salvarModelo(kmeans, 'model_kmeans')

        # Intercluster Distance Maps
        aprendizadoNaoSupervisionado.icdm(data_input, k)
        print("ICDM")

        # Calculated Labels
        # Load model
        loaded_model_Elbow_visualizer = aprendizadoNaoSupervisionado.carregarModelo('model_kmeans')
        y = loaded_model_Elbow_visualizer.labels_
        print(y.shape)

        # Cluster analysis
        # Métricas da validação da Bagging
        n_acertos = aprendizadoNaoSupervisionado.metricasValdacaoBagging(y_encoder, y)
        print("Métricas da validação da Bagging")
        print('Number of acertos: ' + str((n_acertos / len(y_encoder)) * 100))


main = Main()
x_encoder, y_encoder, data_input = main.preProcessamento()
main.visualizacaoScatter(data_input, y_encoder)
main.aprendizadoSupervisionado(x_encoder, y_encoder)
main.aprendizadoNaoSupervisionado(data_input, y_encoder)
