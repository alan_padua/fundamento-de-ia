import numpy as np
from joblib import dump
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split


class AprendizadoSupervisionado:

    def __init__(self):
        self.FOLDER_DUMPS = 'dumps/'
        print("Classe AprendizadoSupervisionado")

    def separarDadosTreinamentoEValidacao(self, x_encoder, y_encoder):
        """
        Depois de fazer o pré-processamento de dados, agora é hora de dividirmos os dados em dados de treinamento e teste
        :param x_encoder:
        :param y_encoder:
        :return: X_train, X_test, Y_train, Y_test
        """

        X_train, X_test, Y_train, Y_test = train_test_split(x_encoder, y_encoder, test_size=0.3, random_state=4)
        # print("X_train shape: {}", format(X_train.shape))
        # print("Y_train shape: {}", format(Y_train.shape))
        # print("X_test shape: {}", format(X_test.shape))
        # print("Y_test shape: {}", format(Y_test.shape))

        return X_train, X_test, Y_train, Y_test

    def regressaoLogistica(self, X_train, Y_train):
        """Regressão logística"""
        conjunto_lr = LogisticRegression(C=0.01, solver='liblinear').fit(X_train, Y_train)

        return conjunto_lr

    def predicao(self, conjunto_lr, X_test):
        y_hat = conjunto_lr.predict(X_test)
        # print(y_hat[:5])

        return y_hat

    def predicaoProbabilidade(self, conjunto_lr, X_test):
        yhat_prob = conjunto_lr.predict_proba(X_test)
        # print(yhat_prob)

        return yhat_prob

    def salvarDados(self, conjunto_lr, nome_arquivo):
        dump(conjunto_lr, self.FOLDER_DUMPS + nome_arquivo)

    def calcularConfusaoMatrix(self, Y_test, y_hat):
        cnf_matrix = confusion_matrix(Y_test, y_hat, labels=[1, 0])
        np.set_printoptions(precision=2)
        return cnf_matrix
