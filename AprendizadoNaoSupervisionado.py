import os
import os.path as path
import pickle
import warnings

import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from yellowbrick.cluster import InterclusterDistance
from yellowbrick.cluster import KElbowVisualizer, SilhouetteVisualizer

warnings.filterwarnings("ignore")

class AprendizadoNaoSuprevisionado:

    def __init__(self):
        self.FOLDER_DUMPS = 'dumps/'
        self.GALLERY = 'gallery'
        print("Classe AprendizadoNaoSupervisionado")

    def newfig(self):
        """Função auxiliar para criar um objeto de eixos das dimensões da galeria."""

        _, ax = plt.subplots(figsize=(15, 10))
        return ax

    def savefig(self, viz, name):
        """Salva a figura no diretório da galeria"""

        if not path.exists(self.GALLERY):
            os.makedirs(self.GALLERY)

        # Must save as png
        if len(name.split(".")) > 1:
            raise ValueError("name should not specify extension")

        outpath = path.join(self.GALLERY, name + ".png")

        viz.show(outpath=outpath)
        # print("created {}".format(outpath))

    def elbow(self, X, max_iter: int, tolerancia: float):
        kmeans = KMeans(max_iter=max_iter, tol=tolerancia)
        Elbow_visualizer = KElbowVisualizer(kmeans, k=(2, 8), ax=self.newfig())
        Elbow_visualizer.fit(X)
        self.savefig(Elbow_visualizer, "elbow")

        # The optimal value of k
        k = Elbow_visualizer.elbow_value_
        print("The optimal value of k:", k)

        return k

    def silhouette(self, X, k):
        kmeans = KMeans(k)
        Silhouette_visualizer = SilhouetteVisualizer(kmeans, ax=self.newfig())
        Silhouette_visualizer.fit(X)
        self.savefig(Silhouette_visualizer, "silhouette")

        return kmeans

    def salvarModelo(self, kmeans, nome_modelo):
        """Salvar o modelo"""
        with open(self.FOLDER_DUMPS + nome_modelo, 'wb') as fp:
            pickle.dump(kmeans, fp)

    def icdm(self, X, k):
        """
         Intercluster DistanceF  Maps
         mostrar distância relativa e tamanho / importância dos clusters

         :param X:
         :param k:
         :return:
        """
        kmeans = KMeans(k)
        InterclusterDistance_visualizer = InterclusterDistance(kmeans, ax=self.newfig())
        InterclusterDistance_visualizer.fit(X)
        self.savefig(InterclusterDistance_visualizer, "icdm")

    def clusterAnalysis(self, y_encoder):
        """
        Métricas da validação da Bagging
        :param y_encoder:

        :return:
        """
        correct_outputs = y_encoder
        n_acertos = 0
        for u in range(0, len(correct_outputs)):
            if y_encoder[u] == correct_outputs[u]:
                n_acertos += 1

        return n_acertos

    def carregarModelo(self, nome_modelo):
        with open(self.FOLDER_DUMPS + nome_modelo, 'rb') as fp:
            pickle.load(fp)

        loaded_model_Elbow_visualizer = pickle.load(open(self.FOLDER_DUMPS + nome_modelo, 'rb'))

        return loaded_model_Elbow_visualizer

    def metricasValdacaoBagging(self, y_encoder, y):
        """Métricas da validação da Bagging"""
        correct_outputs = y_encoder
        n_acertos = 0
        for u in range(0, len(correct_outputs)):
            if y[u] == correct_outputs[u]:
                n_acertos += 1

        return n_acertos
