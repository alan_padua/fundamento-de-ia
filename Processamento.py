import warnings

import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import shuffle

from Graficos import Graficos as graficos

warnings.filterwarnings("ignore")

from sklearn.preprocessing import LabelEncoder


class Processamento:

    def __init__(self, path: str):
        ''' Lendo o arquivo'''
        print("Classe Processamento")
        self.conjunto = pd.read_csv("dataset/healthcare-dataset-stroke-data.csv")

        self.labels = ['sexo', 'idade', 'hipertenso', 'doenca_cardiaca', 'nivel_medio_de_glicose', 'indice_massa_corporal', 'tipo_fumante', 'acidente_vascular_celebral']
        self.labels_x = ['sexo', 'idade', 'hipertenso', 'doenca_cardiaca', 'nivel_medio_de_glicose', 'indice_massa_corporal', 'tipo_fumante']
        self.labels_y = ['acidente_vascular_celebral']

    def alterandoLabels(self):
        """
        Alterando as Labels
        Para ficar mais fácil de entender, traduzindo parar o portugues.
        """
        self.conjunto = self.conjunto.rename(columns={'id': 'id', 'gender': 'sexo', 'age': 'idade', 'hypertension': 'hipertenso', 'heart_disease': 'doenca_cardiaca', 'ever_married': 'nunca_casado',
                                                      'work_type': 'tipo_de_trabalho', 'Residence_type': 'tipo_de_residencia', 'avg_glucose_level': 'nivel_medio_de_glicose',
                                                      'bmi': 'indice_massa_corporal',
                                                      'smoking_status': 'tipo_fumante', 'stroke': 'acidente_vascular_celebral'})

    def removerValores(self):
        """
        Removendo Valores
        - Removendo o valor Other da coluna sexo.
        - Removendo o valor NaN da coluna indice_massa_corporal.

        :return:
        """
        self.conjunto = self.conjunto.drop(self.conjunto[self.conjunto['sexo'] == 'Other'].index)
        self.conjunto = self.conjunto.drop(self.conjunto[self.conjunto['indice_massa_corporal'] == 'N//A'].index)
        # print(self.conjunto.head())

    def removerColunasDesnecessarias(self):
        """
        Mantendo somente as colunas necessária

        :return:
        """
        self.conjunto = self.conjunto[self.labels]

    def encoderTransform(self):
        """
        Preprocessamento

        :return:
        """
        le = LabelEncoder()
        en_conjunto = self.conjunto.apply(le.fit_transform)
        # print(en_conjunto.head())
        return en_conjunto

    def separandoXeY(self, en_conjunto, labels_x: list, labels_y: list):
        """
        Separando x e y
        - X são as colunas para processamento e padrões
        - y são os valores para conferencia e aprendizado
        :param self:
        :param en_conjunto:
        :param labels_x:
        :param labels_y:

        :return:
        """

        X = en_conjunto[labels_x]
        Y = en_conjunto[labels_y]
        return X.values, Y.values

    def dimensionandoFeatures(self, y_encoder, x_encoder):
        """
        Dimensionando cada feature para um determinado intervalo, padrão 0 e 1.
        Esta transformação é freqüentemente usada como uma alternativa para média zero e variância unitária.
        :param y_encoder:
        :param x_encoder:

        :return:
        """
        data_input = MinMaxScaler().fit_transform(x_encoder)
        data_input, y_encoder = shuffle(data_input, y_encoder, random_state=40)

        return data_input, y_encoder

    def visualizarGraficos(self):
        """
        Visualizar gráficos
        
        :return:
        """
        graficos.visualizacaoScatter()
